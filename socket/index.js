import * as config from "./config";
import {
    texts
} from "../data";

// По поводу чистоты кода.. Я не буду придираться, если не будет откровенной жести.

const users = new Map();
const gameRooms = new Map();
const activeGameRooms = new Map();
const gameTextByActiveRooms = new Map();
const winnerList = new Map()

function randomNumber(max) {
    const r = Math.random() * (max);
    return Math.floor(r)
}

function isAllReady(playerList) {
    let allReady = true;
    playerList.forEach(player => {
        if (!player[1])
            allReady = false;
    })
    return allReady;
}

function doFinalList(roomName) {
    const playerListConnected = activeGameRooms.get(roomName);
    let list = winnerList.get(roomName);
    let listRunner = new Map();
    playerListConnected.forEach((value, key) => {
        if (value != "")
            listRunner.set(key, value);
    })
    listRunner = new Map([...listRunner.entries()].sort((a, b) => -b[1].length + a[1].length))
    listRunner.forEach((value, key) => {
        list.push(key);
    })
    list = list.filter(elem => playerListConnected.has(elem));
    return list;
}

function getAvailableRooms(gameRooms) {
    const availableRooms = new Map();
    gameRooms.forEach((value, key) => {
        let allReady = isAllReady(value);
        if (value.length < 5 && !(allReady && value.length > 0))
            availableRooms.set(key, value)
    })
    return availableRooms;
}

function gameStart(socket, roomName) {
    const randomID = randomNumber(texts.length)
    const gameText = texts[randomID];

    gameTextByActiveRooms.set(roomName, gameText);
    activeGameRooms.set(roomName, new Map());

    gameRooms.get(roomName).forEach(player => {
        activeGameRooms.get(roomName).set(player[0], gameText);
    })
    winnerList.set(roomName, []);
    socket.emit("GAME_START", randomID);
    socket.to(roomName).emit("GAME_START", randomID);
    let count = config.SECONDS_TIMER_BEFORE_START_GAME - 1;
    socket.emit("UPDATE_TIMER", count + 1);
    const startTimer = setInterval(() => {
        if (count == 0) {
            clearInterval(startTimer);
            mainGame(socket, roomName);
        } else if (!gameRooms.has(roomName))
            clearInterval(startTimer);
        else {
            socket.emit("UPDATE_TIMER", count);
            socket.to(roomName).emit("UPDATE_TIMER", count);
            count--;
        }
    }, 1000);
}

function mainGame(socket, roomName) {
  if(activeGameRooms.has(roomName)){
    socket.emit("MAIN_GAME_START");
    socket.to(roomName).emit("MAIN_GAME_START");
    let count = config.SECONDS_FOR_GAME - 1;
    socket.emit("UPDATE_TIMER", `${count+1} left`);
    const mainTimer = setInterval(() => {        
        if(activeGameRooms.has(roomName)){
        let allWin = true;
        activeGameRooms.get(roomName).forEach((value, key) => {
            if (value != "")
                allWin = false;
        })
        if (count == 0 || !gameRooms.has(roomName)) {
            clearInterval(mainTimer);
            if (gameRooms.has(roomName)) {
                const rating = doFinalList(roomName);
                socket.emit("GAME_END", rating);
                socket.to(roomName).emit("GAME_END", rating);
                const updatedListPlayers = []
                gameRooms.get(roomName).forEach(player => {
                    updatedListPlayers.push([player[0], false]);
                })
                gameRooms.delete(roomName);
                gameRooms.set(roomName, updatedListPlayers)
                activeGameRooms.delete(roomName);
                socket.emit("UPDATE_PLAYERS", gameRooms.get(roomName))
                socket.to(roomName).emit("UPDATE_PLAYERS", gameRooms.get(roomName))
                const data = getAvailableRooms(gameRooms)
                socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);
            }
        } else if (allWin) {
            clearInterval(mainTimer);
            const rating = doFinalList(roomName);
            socket.emit("GAME_END", rating);
            socket.to(roomName).emit("GAME_END", rating);
            const updatedListPlayers = []
            gameRooms.get(roomName).forEach(player => {
                updatedListPlayers.push([player[0], false]);
            })
            gameRooms.delete(roomName);
            gameRooms.set(roomName, updatedListPlayers)

            activeGameRooms.delete(roomName);

            socket.emit("UPDATE_PLAYERS", gameRooms.get(roomName))
            socket.to(roomName).emit("UPDATE_PLAYERS", gameRooms.get(roomName))
            const data = getAvailableRooms(gameRooms)
            socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);
        } else {
            socket.emit("UPDATE_TIMER", `${count} left`);
            socket.to(roomName).emit("UPDATE_TIMER", `${count} left`);
            count--;
        }
    }
    else{
        clearInterval(mainTimer);
    }
    }, 1000)
  }
}
export default io => {

    io.on("connection", socket => {
        const username = socket.handshake.query.username;
        if (users.has(username)) {
            socket.emit("USERNAME_ALREADY_TAKEN")
        } else {
            users.set(username, "waiting_room");
            const data = getAvailableRooms(gameRooms);
            socket.emit("UPDATE_ROOM", [...data])
            socket.join("waiting_room")
            socket.on("PUT_LETTER", inf => {
                const roomName = inf[0];
                if(activeGameRooms.has(roomName)){
                const letter = inf[1];
                const playerList = activeGameRooms.get(roomName)
                const textLength = gameTextByActiveRooms.get(roomName).length
                const unwrittenText = playerList.get(username);
                const countWrittenLetter = textLength - unwrittenText.length
                if (letter == unwrittenText[0]) {


                    let sizeBar = (countWrittenLetter + 1) / textLength;
                    socket.emit("UPDATE_BAR", [username, sizeBar]);
                    socket.to(roomName).emit("UPDATE_BAR", [username, sizeBar]);
                    playerList.set(username, unwrittenText.substr(1));
                    if (unwrittenText.substr(1) == "") {
                        winnerList.get(roomName).push(username);
                        playerList.set(username, "");
                    }
                    socket.emit("LETTER_APPROVED");
                }
            }
            })




            socket.on("ADD_NEW_ROOM", information => {
                if (gameRooms.has(information.roomName))
                    socket.emit("ROOM_NAME_ALREADY_TAKEN")
                else {
                    gameRooms.set(information.roomName, [])
                    socket.leave("waiting_room");
                    socket.join(information.roomName);
                    gameRooms.get(information.roomName).push([information.userName, false]);
                    users.set(username, information.roomName)
                    socket.emit("ADD_TO_ROOM", [information.roomName, gameRooms.get(information.roomName)])

                    const data = getAvailableRooms(gameRooms);
                    socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);
                }
            })
            socket.on("LEAVE_ROOM", () => {
                const roomName = users.get(username)
                users.set(username, "waiting_room")
                if(gameRooms.has(roomName)){
                const listPlayers = gameRooms.get(roomName)
                const index = listPlayers.findIndex(elem => elem[0] == username);
                listPlayers.splice(index, 1);
                if (listPlayers.length == 0)
                    gameRooms.delete(roomName);
                const data = getAvailableRooms(gameRooms);
                socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);
                socket.to(roomName).emit("UPDATE_PLAYERS", gameRooms.get(roomName))
                if (listPlayers.length > 0 && isAllReady(listPlayers))
                  gameStart(socket, roomName, username);
                socket.leave(roomName);
                  socket.join("waiting_room")
                  socket.to(roomName).emit("UPDATE_PLAYERS", gameRooms.get(roomName))
                  socket.emit("UPDATE_ROOM", [...data]);
              }

            })

            socket.on("JOIN_TO_ROOM", information => {
                users.set(username, information.roomName)
                socket.leave("waiting_room");
                socket.join(information.roomName);
                gameRooms.get(information.roomName).push([information.userName, false]);

                socket.emit("ADD_TO_ROOM", [information.roomName, gameRooms.get(information.roomName)])

                const data = getAvailableRooms(gameRooms);
                socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);

                socket.to(users.get(username)).emit("UPDATE_PLAYERS", gameRooms.get(users.get(username)))
            })
            socket.on("CHANGED_STATUS", () => {
              if(gameRooms.has(users.get(username))){
                gameRooms.get(users.get(username)).forEach(
                    player => {
                        if (player[0] == username)
                            player[1] = !player[1];
                    }
                )
                const players = gameRooms.get(users.get(username));
                if (isAllReady(players)) {
                    const data = getAvailableRooms(gameRooms);
                    socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);
                    if (players.length) {
                        gameStart(socket, users.get(username), username);
                    }
                }
                socket.to(users.get(username)).emit("CHANGE_PLAYER_STATUS", username);
              }
            })




            socket.on("disconnect", () => {
                if (users.has(username)) {
                    const roomName = users.get(username);
                    if (activeGameRooms.has(roomName)) {
                        const listPlayers = activeGameRooms.get(roomName)
                        listPlayers.delete(username);
                        if (listPlayers.size == 0)
                            activeGameRooms.delete(roomName);
                    }
                    if (gameRooms.has(roomName)) {
                        const listPlayers = gameRooms.get(roomName)
                        const index = listPlayers.findIndex(elem => elem[0] == username);
                        listPlayers.splice(index, 1);
                        if (listPlayers.length == 0)
                            gameRooms.delete(roomName);
                        const data = getAvailableRooms(gameRooms);
                        socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);

                        socket.to(roomName).emit("UPDATE_PLAYERS", gameRooms.get(roomName))
                    }
                    if (gameRooms.has(roomName)) {
                        gameRooms.get(users.get(username)).forEach(
                            player => {
                                if (player[0] == username)
                                    player[1] = !player[1];
                            }
                        )
                        const players = gameRooms.get(users.get(username));
                        if (isAllReady(players)) {
                            const data = getAvailableRooms(gameRooms);
                            socket.to("waiting_room").emit("UPDATE_ROOM", [...data]);
                            if (players.length&&!activeGameRooms.has(users.get(username))) {
                                gameStart(socket, users.get(username), username);
                            }
                        }
                    }
                    socket.leave(roomName);
                    users.delete(username)
                }
            })
        }
    });
};
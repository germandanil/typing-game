const username = sessionStorage.getItem("username");
const socket = io("", {
    query: {
        username
    }
});
let TEXT = ""
let room = "waiting_room"

const readyBtn = document.getElementById("ready-btn");
const textContainer = document.getElementById("text-container");
const timer = document.getElementById("timer");
const quitResultBtn = document.getElementById("quit-results-btn")
const addRoomBtn = document.getElementById("add-room-btn");
const quitRoomBtn = document.getElementById("quit-room-btn");
const roomsPage = document.getElementById("rooms-page")
const roomsName = document.getElementById("room-name")
const gamePage = document.getElementById("game-page")
const ratingElem = document.getElementById("rating")




setInterval(() => {
    socket.emit("GET_USERS_COUNT")
}, 100)



function addLetterHandler(event) {
    socket.emit("PUT_LETTER", [room, event.key]);
}

function createGameText(text) {
    textContainer.innerHTML = ""
    for (let i = 0; i < text.length; i++) {
        let letter = text.charAt(i);
        const span = document.createElement("span");
        if (i == 0)
            span.className = "next"
        else
            span.className = "unwritten";
        span.innerHTML = letter;
        textContainer.append(span);
    }
}

function createPlayer(playerInf) {
    const player_wrp = document.createElement("div");
    player_wrp.className = "player-wrp"

    const player = document.createElement("div");
    player.classList.add("player", "flex");

    const user_progress_wrp = document.createElement("div");
    user_progress_wrp.className = "user-progress-wrp"

    const circle = document.createElement("div");
    circle.classList.add("circle");
    circle.id = `status_${playerInf[0]}`
    if (playerInf[1])
        circle.classList.add("ready-status-green")
    else
        circle.classList.add("ready-status-red")
    const name = document.createElement("p");
    name.className = "name"
    name.innerHTML = playerInf[0];
    if (username == playerInf[0])
        name.innerHTML += " (you)"
    const bar = document.createElement("div");
    bar.classList.add("user-progress", playerInf[0])
    player_wrp.append(player, user_progress_wrp);
    player.append(circle, name);
    user_progress_wrp.append(bar);

    document.getElementById("players-list").append(player_wrp);

}

function createRoom(roomID, length) {
    const roomElem = document.createElement("div");
    roomElem.className = "room";
    const p = document.createElement("p");
    p.className = "count-in-room"
    p.innerHTML = `${length} user conected`;
    const h = document.createElement("h3");
    h.className = "room-name"
    h.innerHTML = roomID;
    const btn = document.createElement("button");
    btn.className = "join-btn"
    btn.id = `room_${roomID}`
    btn.innerHTML = "Join";
    roomElem.append(p, h, btn);
    document.getElementById("rooms").append(roomElem);
}

function preparation_for_login() {
    sessionStorage.clear();
    window.alert("This username is already taken!")
    window.location.replace("/login");
}
if (!username) {
    window.location.replace("/login");
}

quitResultBtn.addEventListener("click", () => {
    ratingElem.classList.add("display-none");
    timer.classList.add("display-none");
    timer.classList.remove("game-counter")
    readyBtn.classList.remove("display-none");
    readyBtn.classList.remove("btn-ready-status-green")
    readyBtn.classList.remove("btn-ready-status-red")
    readyBtn.innerHTML = "NOT READY"
    quitRoomBtn.classList.remove("hidden")
    textContainer.innerHTML = ""
    textContainer.classList.add("display-none")
    readyBtn.className = "btn-ready-status-red"

    document.querySelectorAll("user-progress").forEach(elem => elem.style.width = "0px")
});


addRoomBtn.addEventListener("click", () => {
    const result = window.prompt("Enter room name:");
    if (result) {
        socket.emit("ADD_NEW_ROOM", {
            userName: username,
            roomName: result
        });
    }
})
readyBtn.addEventListener("click", () => {
    const statusReady = document.getElementById(`status_${username}`);
    statusReady.className = "circle"
    if (readyBtn.className == "btn-ready-status-red") {
        readyBtn.className = "btn-ready-status-green"
        readyBtn.innerHTML = "READY"
        statusReady.classList.add("ready-status-green");
    } else {
        readyBtn.className = "btn-ready-status-red"
        readyBtn.innerHTML = "NOT READY"
        statusReady.classList.add("ready-status-red");
    }
    socket.emit("CHANGED_STATUS");
})
quitRoomBtn.addEventListener("click", () => {
    gamePage.classList.add("display-none");
    roomsPage.classList.remove("display-none");
    socket.emit("LEAVE_ROOM");
    room = "waiting_room"
})



socket.on("MAIN_GAME_START", () => {
    timer.classList.remove("start-counter")
    timer.classList.add("game-counter");
    textContainer.classList.remove("display-none");
    createGameText(TEXT);
    document.addEventListener("keydown", addLetterHandler)
})
socket.on("LETTER_APPROVED", () => {
    document.querySelector(".next").className = "written";
    if (document.querySelector(".unwritten"))
        document.querySelector(".unwritten").className = "next";
});
socket.on("UPDATE_BAR", playerInf => {
    const elem = document.querySelector(`.${playerInf[0]}`);
    elem.style.width = `${playerInf[1]*100}%`
    if (playerInf[1] == 1)
        elem.style.backgroundColor = "green";
})
socket.on("GAME_END", rating => {
    ratingElem.classList.remove("display-none")
    let numPlace = 0
    const bodyPlaceElement = document.getElementById("raiting-main");
    bodyPlaceElement.innerHTML = ""
    rating.forEach(name => {
        numPlace++;
        const place = document.createElement("p")
        place.className = `place-${name}`
        place.innerHTML = `#${numPlace}. ${name}`;
        bodyPlaceElement.append(place);
    })
    document.removeEventListener("keydown", addLetterHandler);
})
socket.on("GAME_START", id => {
    const host = window.location.hostname;
    let src = `${window.location.host}/game/texts/${id}`
    console.log(host);
    if(host == "localhost")
        src ="http://" + src;
    else src = "https://" + src;
    fetch(src).then(
        res => res.text()
    ).then(res => {
        TEXT = res;
        document.getElementById("text-container").innerHTML = TEXT;

    })

    quitRoomBtn.classList.add("hidden");
    readyBtn.classList.add("display-none");
    timer.classList.remove("display-none");
    timer.classList.add("start-counter");
    timer.innerHTML = ""
})
socket.on("UPDATE_TIMER", count => {
    timer.innerHTML = `${count}`
})


socket.on("CHANGE_PLAYER_STATUS", username => {
    const status_player = document.getElementById(`status_${username}`)
    console.log(status_player);
    if (status_player.classList.contains("ready-status-red")) {
        status_player.classList.remove("ready-status-red");
        status_player.classList.add("ready-status-green")
    } else {
        status_player.classList.remove("ready-status-green");
        status_player.classList.add("ready-status-red")
    }
})

socket.on("UPDATE_PLAYERS", players => {
    document.getElementById("players-list").innerHTML = ""
    players.forEach(player => {
        createPlayer(player);
    })
})


socket.on("ADD_TO_ROOM", infromation => {
    document.getElementById("players-list").innerHTML = ""
    roomsPage.classList.add("display-none");
    gamePage.classList.remove("display-none")
    roomsName.innerHTML = infromation[0];
    room = infromation[0];
    infromation[1].forEach(player => {
        createPlayer(player);
    })
})
socket.on("USERNAME_ALREADY_TAKEN", preparation_for_login)
socket.on("UPDATE_ROOM", arrayRooms => {
    document.getElementById("rooms").innerHTML = "";
    arrayRooms.forEach(element => {
        createRoom(element[0], element[1].length)
    });
    document.querySelectorAll(".join-btn").forEach(btn => {
        btn.addEventListener("click", () => {
            socket.emit("JOIN_TO_ROOM", {
                userName: username,
                roomName: btn.id.substr(5)
            })
        })
    })

})
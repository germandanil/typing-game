import { Router } from "express";
import texts from "../data"

const router = Router();

router
  .get("/:id", (req, res) => {
    const ID  = req.params.id
    res.status(200).send(texts.texts[ID]);
  });

export default router;
